# _*_ coding:utf-8 _*_
# https://www.cnblogs.com/will-wu/p/14731728.html
# 定时器

from apscheduler.schedulers.background import BackgroundScheduler
import datetime
import time


# 获取时间区间
def time_range(minutes=20):
    current_time = datetime.datetime.now()
    start_time = (current_time - datetime.timedelta(minutes=minutes)).strftime("%Y-%m-%d %H:%M:%S")
    end_time = current_time.strftime("%Y-%m-%d %H:%M:%S")
    return [start_time, end_time]
    # print(start_time)
    # print(end_time)


if __name__ == '__main__':
    timing_type = BackgroundScheduler(timezone='MST')
    # 在start_date到end_date时间段内，每隔5s执行一次print_minutes_now；其中，func=这些可以省略，start_date和end_date可以单独使用，
    # 在end_date之前，每隔0.2小时运行一次print_minutes_now
    timing_type.add_job(func=time_range, trigger='interval', seconds=10, start_date='2022-01-01')
    # 启动调度器
    timing_type.start()
    while(True):
        time.sleep(60*20)