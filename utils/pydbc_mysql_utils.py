# _*_ coding:utf-8 _*_
# 导入pymysql模块
import pymysql
from utils.log_utils import logger
# 连接database
conn = pymysql.connect(host='127.0.0.1', user='root', password='123456', database='fei_ge_ke_fu', charset='utf8')
# 得到一个可以执行SQL语句的光标对象
cursor = conn.cursor()


# 查询
def query(sql='select 1', args=[]):
    # username = "Alex"
    # age = 18
    # 执行SQL语句
    cursor.execute(sql, args)
    logger.info('执行sql>> ' + sql)
    logger.info('参数>> ' + str(args))
    ret = cursor.fetchall()
    return ret
    # print(ret[0][4])
    # 提交事务
    # conn.commit()
    # cursor.close()
    # conn.close()


# insert
def insert(sql='', args=[]):
    try:
        logger.info('执行sql>> ' + sql)
        logger.info('参数s>> ' + str(args))
        # 执行SQL语句
        cursor.execute(sql, args)
        # 提交事务
        conn.commit()
    except Exception as e:
        # 有异常，回滚事务
        conn.rollback()


if __name__ == '__main__':
    sql = "select * from  message"
    # query(sql)
    # query(sql)
    # insert_sql = "INSERT INTO message(`shop_name`, `user_name`,`message_content`) VALUES (%s,%s,%s)"
    # insert(insert_sql, ['wj', '123', '456'])
