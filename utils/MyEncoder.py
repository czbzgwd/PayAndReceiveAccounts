import scipy.io as sio
import os
import json
import numpy as np

load_fn = '2%.mat'
load_data = sio.loadmat(load_fn)
print(load_data.keys())


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, bytes):
            return str(obj, encoding='utf-8');
        return json.JSONEncoder.default(self, obj)


save_fn = os.path.splitext(load_fn)[0] + '.json'
file = open(save_fn, 'w', encoding='utf-8');
file.write(json.dumps(load_data, cls=MyEncoder, indent=4))
file.close()
