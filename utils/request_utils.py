# _*_ coding:utf-8 _*_
# 1.导包
import base64

import requests

from utils.ocr_util import invoice_util


def post(url='', json={}, headers={}):
    # 请求的URL
    # url = 'http://weixin.voiceads.cn/apiNotice?t=v89qN9S3v2uMMJG'
    # # 请求头
    # headers = {'content-type': 'application/json'}
    # # 请求的参数
    # # json = {
    # #     "data": [{'flag': 'mobile',
    # #               'password': 'e9f5c5240c0bb39488e6dbfbdb1517e0',
    # #               'mobile_phone': ' ********'}]
    # # }
    # json = {
    #     "data": "eyAgICJzdW1tYXJ5Ijoi5Lia5Yqh5pGY6KaBIiwgICAiYnVzaW5lc3NUeXBlIjoi5Lia5Yqh57G75Z6LIiwgICAiYnVzaW5lc3NTdGF0dXMiOiLkuJrliqHnirbmgIEiLCAgICJidXNpbmVzc0NvbnRlbnQiOiLkuJrliqHlhoXlrrkiLCAgICJvcGVuaWQiOlsgICAgICAgIm80LXVSam15M0VKX1lDNjdhVVdPc1lMbVROUEUiICAgXSwgICAicmVtYXJrIjoi5aSH5rOoIiwgICAidXJsIjoi6Lez6L2s5Zyw5Z2AIiB9"
    # }
    r = requests.post(url, json=json, headers=headers)
    # 3.获取响应对象
    # print(r.text)  # 文本格式
    # print(r.json())  # json格式
    # # 4.获取响应状态码
    # print(r.status_code)
    # # 5.请求的URL
    # print(r.url)


if __name__ == "__main__":
    # iflyTekUrl = "http://iflydigita.api.xf-yun.com/v1/private/se9b136aa"
    # # 读取发票图片
    # path = "D://test.png"
    # w = open(path, 'rb')
    # t = base64.b64encode(w.read())
    # w.close()
    # # print(t)
    # headers = {'content-type': 'application/json'}
    # params = {
    #     "image_data": t,
    #     "format": "format"
    # }
    # c = requests.post(iflyTekUrl, params=params, headers=headers)
    # print(c)
    # print('123')
    # st = 'eyJyZXN1bHQiOjEsInJlc3BvbnNlIjp7ImRhdGEiOnsicmVzdWx0IjoxLCJzaGExIjoiMVNSU2tIVjZSMWlrSXI3ZUt1M2ZkODAwIiwidGltZXN0YW1wX3JlcXVlc3RfYXJyaXZhbCI6MTY1MjQzMjQyMjIwMSwiZXh0cmEiOnsiYmFyY29kZSI6W119LCJ0aW1lX2Nvc3QiOiIxMDY5IiwiaWRlbnRpZnlfcmVzdWx0cyI6W3sib3JpZW50YXRpb24iOjAsImRldGFpbHMiOnsiZGF0ZSI6IjIwMjLlubQwMuaciDIy5pelIiwic2VsbGVyIjoi56eR5aSn6K6v6aOe6IKh5Lu95pyJ6ZmQ5YWs5Y+4IiwiY29kZSI6IjM0MDAyMTMxMzAiLCJjb21wYW55X3NlYWwiOiIxIiwiY2l0eSI6IiIsInJlbWFyayI6IkVDUzIwMjJLRFhGMDIxNTAxIiwiaXRlbV9uYW1lcyI6IirmlofljJblip7lhaznlKjorr7lpIcq6K6v6aOe5Y+M5bGP57+76K+RXG7mnLosKui9r+S7tirorq/po57lj4zlsY/nv7vor5HmnLrova/ku7bns7vnu59cblYxLjAiLCJmb3JtX3R5cGUiOiIiLCJibG9ja19jaGFpbiI6IjAiLCJ0aXRsZSI6IuWuieW+veWinuWAvOeojuS4k+eUqOWPkeelqCIsInRheF9yYXRlIjoiMTMlIiwiaXNzdWVyIjoi5p2O6ZuqIiwic2VsbGVyX3RheF9pZCI6IjkxMzQwMDAwNzExNzcxMTQzSiIsImJ1eWVyX3RheF9pZCI6IjkxNTAwMTA1NTg4OTM5MjYxSCIsIm51bWJlciI6IjA5MDE4MTgxIiwidG90YWxfY24iOiLlo7nkvbDlo7nmi77ogobkuIflnIbmlbQiLCJ0b3RhbCI6IjExNDAwMDAuMDAiLCJvaWxfbWFyayI6IjAiLCJwcm92aW5jZSI6IiIsIm51bWJlcl9jb25maXJtIjoiMDkwMTgxODEiLCJwcmV0YXhfYW1vdW50IjoiMTAwODg0OS41NiIsImJ1c19wYXNzZW5nZXIiOm51bGwsInNlbGxlcl9iYW5rX2FjY291bnQiOiLkuqTpgJrpk7booYzlkIjogqXpq5jmlrDljLrmlK/ooYwzNDEzMTMwMDAwMTAxNDEwMDA0ODMiLCJidXllcl9iYW5rX2FjY291bnQiOiLkuK3lm73lt6XllYbpk7booYzph43luobluILkuIrmuIXlr7rmlK/ooYwzMTAwMDIxNzE5MjAwMDg4Mzg5IiwiaW5wdXRfdGF4X3JhdGUiOjAuMTMwMCwiY2lwaGVydGV4dCI6IioqMDkvODA8MTUvMjkwMjM3MTQvPjYvODU2NiwyKjMvKz42Pj48MzE8OTgrMiszNDExNDA5NDksLzMwNDQ4Mzk5Lz42PisvPjkwNjQ2MTUxLTEqLDg0NisxPDEqMzcvKjc1Ly0rLSs8MjI4LTAiLCJjb2RlX2NvbmZpcm0iOiIzNDAwMjEzMTMwIiwia2luZCI6IiIsInRheCI6IjEzMTE1MC40NCIsInJldmlld2VyIjoi5YiY5pmT6Iy5IiwiYnV5ZXIiOiLkvJ/ku5XkvbPmnbAo6YeN5bqGKeenkeaKgOaciemZkOWFrOWPuCIsImJ1eWVyX2FkZHJfdGVsIjoi6YeN5bqG5biC5rGf5YyX5Yy65YyX5ruo5LqM6LevNDYy5Y+3Ny0xLDctMjAyMy02NzYyODgwMSIsImlucHV0X3RheCI6IjEzMTE1MC40NCIsInJlY2VpcHRvciI6IuipueWwj+WkqSIsInNlbGxlcl9hZGRyX3RlbCI6IuWQiOiCpeW4gumrmOaWsOW8gOWPkeWMuuacm+axn+ilv+i3rzY2NuWPtzA1NTEtNjUzMDkzNjYiLCJpdGVtcyI6W3sidW5pdCI6IuaUryIsInRvdGFsIjoiNTQ4NjcyLjU3IiwicXVhbnRpdHkiOiI0MDAiLCJwcmljZSI6IjEzNzEuNjgxNDI1IiwibmFtZSI6IirmlofljJblip7lhaznlKjorr7lpIcq6K6v6aOe5Y+M5bGP57+76K+RXG7mnLoiLCJzcGVjaWZpY2F0aW9uIjoiVklULVAxMCIsInRheCI6IjcxMzI3LjQzIiwidGF4X3JhdGUiOiIxMyUifSx7InVuaXQiOiLlpZciLCJ0b3RhbCI6IjQ2MDE3Ni45OSIsInF1YW50aXR5IjoiNDAwIiwicHJpY2UiOiIxMTUwLjQ0MjQ3NSIsIm5hbWUiOiIq6L2v5Lu2Kuiur+mjnuWPjOWxj+e/u+ivkeacuui9r+S7tuezu+e7n1xuVjEuMCIsInNwZWNpZmljYXRpb24iOiJWMS4wIiwidGF4IjoiNTk4MjMuMDEiLCJ0YXhfcmF0ZSI6IjEzJSJ9XSwiZm9ybV9uYW1lIjoi5oq15omj6IGUIiwidHJhbnNpdF9tYXJrIjoiMCJ9LCJ0eXBlIjoiMTAxMDAiLCJyZWdpb24iOlsxMDYsNDQsMTM4Niw3OTVdLCJleHRyYSI6bnVsbH1dLCJ0aW1lc3RhbXBfaW1hZ2VfcmVjZWl2ZWQiOjE2NTI0MzI0MjIyMDEsImlkIjoiNjhmMmY1ZjItZTc0NC00M2JlLWE0ZTUtOGJkZmY2NzRjOTUxIiwibWVzc2FnZSI6InN1Y2Nlc3MiLCJ0b3RhbF90aW1lIjoiMTA2OSIsInZlcnNpb24iOiIxLjAuMTAwNy0yLjAuMjAyMCIsInRpbWVzdGFtcCI6MTY1MjQzMjQyMjIwMX19fQ=='
    # print(base64.b64decode(st).decode('utf-8'))

    invoice_util()
