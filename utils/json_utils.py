
import json


# 通过路径获取json对象
def get_json_by_path(path='', encoding='utf-8'):
    try:
        with open(path, 'r', encoding=encoding) as f:
            json_obj = json.load(f)
        return json_obj
    except:
        return False


def set_json_to_path(path='', json_data="{}", encoding='utf-8'):
    try:
        with open(path, "w", encoding=encoding) as fp:
            json.dump(json_data, fp, ensure_ascii=False)
        return True
    except:
        return False


if __name__ == '__main__':
    obj = get_json_by_path(r'D:\A_pycharm\open_page\config\feige_config.json')
    p = obj['log_path']
    print(p)

