{
    "result": 1,
    "response": {
        "data": {
            "result": 1,
            "sha1": "1Vnq1EY38rDCKY3i1H0epM00",
            "timestamp_request_arrival": 1652585802173,
            "extra": {
                "barcode": []
            },
            "time_cost": "1073",
            "identify_results": [
                {
                    "orientation": 0,
                    "details": {
                        "date": "2022年02月22日",
                        "seller": "科大讯飞股份有限公司",
                        "code": "3400213130",
                        "company_seal": "1",
                        "city": "",
                        "remark": "ECS2022KDXF021501",
                        "item_names": "*文化办公用设备*讯飞双屏翻译\n机,*软件*讯飞双屏翻译机软件系统\nV1.0",
                        "form_type": "",
                        "block_chain": "0",
                        "title": "安徽增值税专用发票",
                        "tax_rate": "13%",
                        "issuer": "李雪",
                        "seller_tax_id": "91340000711771143J",
                        "buyer_tax_id": "91500105588939261H",
                        "number": "09018181",
                        "total_cn": "壹佰壹拾肆万圆整",
                        "total": "1140000.00",
                        "oil_mark": "0",
                        "province": "",
                        "number_confirm": "09018181",
                        "pretax_amount": "1008849.56",
                        "bus_passenger": null,
                        "seller_bank_account": "交通银行合肥高新区支行341313000010141000483",
                        "buyer_bank_account": "中国工商银行重庆市上清寺支行3100021719200088389",
                        "input_tax_rate": 0.13,
                        "ciphertext": "**09/80<15/29023714/>6/8566,2*3/+>6>><31<98+2+341140949,/30448399/>6>+/>90646151-1*,846+1<1*37/*75/-+-+<228-0",
                        "code_confirm": "3400213130",
                        "kind": "",
                        "tax": "131150.44",
                        "reviewer": "刘晓茹",
                        "buyer": "伟仕佳杰(重庆)科技有限公司",
                        "buyer_addr_tel": "重庆市江北区北滨二路462号7-1,7-2023-67628801",
                        "input_tax": "131150.44",
                        "receiptor": "詹小天",
                        "seller_addr_tel": "合肥市高新开发区望江西路666号0551-65309366",
                        "items": [
                            {
                                "unit": "支",
                                "total": "548672.57",
                                "quantity": "400",
                                "price": "1371.681425",
                                "name": "*文化办公用设备*讯飞双屏翻译\n机",
                                "specification": "VIT-P10",
                                "tax": "71327.43",
                                "tax_rate": "13%"
                            },
                            {
                                "unit": "套",
                                "total": "460176.99",
                                "quantity": "400",
                                "price": "1150.442475",
                                "name": "*软件*讯飞双屏翻译机软件系统\nV1.0",
                                "specification": "V1.0",
                                "tax": "59823.01",
                                "tax_rate": "13%"
                            }
                        ],
                        "form_name": "抵扣联",
                        "transit_mark": "0"
                    },
                    "type": "10100",
                    "region": [
                        106,
                        44,
                        1386,
                        795
                    ],
                    "extra": null
                }
            ],
            "timestamp_image_received": 1652585802173,
            "id": "515f6ccc-f8bb-4341-8512-e6d331c00604",
            "message": "success",
            "total_time": "1073",
            "version": "1.0.1007-2.0.2020",
            "timestamp": 1652585802173
        }
    }
}