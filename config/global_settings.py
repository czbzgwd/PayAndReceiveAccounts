
# 测试地址
# URL = 'https://bzrobotdev.iflysec.com/wms'

ACTIVE = 'dev'

if 'dev' == ACTIVE:
    # 共享作业平台
    SHARE_URL = 'http://172.30.10.154/arapshare/#/login'

    # 数智财经管理平台
    ARAP_URL = 'http://172.30.10.154/arapfinance/#/login'

    API_ADD_URL = f"http://172.30.10.154:32000/arap/receive/post/save"

    # 上传文件
    API_UPLOAD = f"http://172.30.10.154:32000/arap/file/upload"

    # RPA监听该接口，如果有返回值那么RPA就往下执行
    API_CONFIRM_URL = f"http://172.30.10.154:32000/arap/receive/get/rpa/list"

    # rpa状态同步接口
    API_SYNC_URL = f"http://172.30.10.154:32000/arap/receive/get/change/rpa/status"

    # 清除应收，还原待办接口
    API_CLEAR_URL = f"http://172.30.10.154:32000/arap/receive/get/clear"

    # 账龄预警数据初始化
    INIT_URL = f"http://172.30.10.154:32000/arap/agingWarning/get/init"

elif 'test' == ACTIVE:
    # 共享作业平台
    SHARE_URL = 'http://172.30.10.107/arapshare/#/login'

    # 数智财经管理平台
    # 登录
    ARAP_URL = 'http://172.30.10.107/arapfinance/#/login'
    # 新增数据
    API_ADD_URL = f"http://172.30.10.107:32000/arap/receive/post/save"
    # 上传文件
    API_UPLOAD = f"http://172.30.10.107:32000/arap/file/upload"

    # RPA监听该接口，如果有返回值那么RPA就往下执行
    API_CONFIRM_URL = f"http://172.30.10.107:32000/arap/receive/get/rpa/list"

    # rpa状态同步接口
    API_SYNC_URL = f"http://172.30.10.107:32000/arap/receive/get/change/rpa/status"

    # 清除应收，还原待办接口
    API_CLEAR_URL = f"http://172.30.10.107:32000/arap/receive/get/clear"

    # 账龄预警数据初始化
    INIT_URL = f"http://172.30.10.107:32000/arap/agingWarning/get/init"


OFFICIAL_URL = "https://fip.cscec.com/cas/login?locale=zh_CN&service=https%3A%2F%2Ffip.cscec.com%2FOSPPortal" \
               "%2FCSCPortal.jsp%3Ftdsourcetag%3Ds_pctim_aiomsg%26locale=zh_CN"  # 官网地址
# 共享平台用户名和密码
gx_username = 'gxuser'
gx_password = '123456'

# 智能财经平台用户名和密码
cj_username = 'finuser'
cj_password = '123456'

# 环境配置
ENVIRON_PATH = r"C:\RPA_CHROME\environ.json"

# 指定chrome驱动路径
CHROME_PATH = r"C:\RPA_CHROME\chromedriver.exe"

# 页面操作默认等待时间
DEFAULT_WAIT = 30

# RPA填报附件/发票下载路径
ENTRY_DOWNLOAD_PATH = r'D:\RPA_DOWNLOAD'

# 附件下载路径
YFYS_DOWNLOAD_PATH = r'C:\Users\admin\Downloads'

# 上传的文件所在的路径
UPLOAD_PATH = r"C:\RPA_CHROME"

# 八局一体化系统支持的附件格式
SUPPORT_FJ = ['.pdf', '.docx', '.doc', '.xls', '.xlsx','.png']

# 增值税发票综合服务平台(安徽)
FPDK_URL = 'https://fpdk.anhui.chinatax.gov.cn/sigin.c9e04d4b.html'

# 金税盘密码
jsp_password = '12345678'

