# -*- coding: utf-8 -*-

import os
import time
import json
from pywinauto import Desktop

from config.global_settings import ENTRY_DOWNLOAD_PATH, URL, username, password
from yfys.original_robot import OriginalRobot
from zjbj.web_util.web_robot import WebRobot


class YfysRobotTest(OriginalRobot):
    def __init__(self, url, download_path):
        """
        初始化
        :param url: 八局url
        :param download_path: 附件/发票下载路径
        :param body: 填报信息
        """
        super().__init__(url, download_path)
        self.download_path = download_path
        self.url = url

    def run(self):
        # 清除之前下载的文件
        # self.clear_file()

        # 登录
        self.login()

        # 导出文件
        self.export()

        # 上传
        self.upload()

        self.getDataFromWebPage()

        self.getDataFromJsonFile()

    def login(self):
        # 打开网页
        self.driver.get(self.url)
        # 输入用户名和密码
        self.fill_input('//input[@id="username"]', username)
        self.fill_input('//input[@id="password"]', password)
        self.click_element('//button', mode='internal')
        time.sleep(1)

    def export(self):
        time.sleep(1)
        self.click_element('//a[contains(@href,"warning")]', mode='internal')
        time.sleep(0.5)
        self.click_element('//span[text()="导 出"]//parent::button', mode='internal')
        time.sleep(0.5)
        self.click_element('//span[text()="确 定"]//parent::button', mode='internal')

    def clear_file(self):
        """
        root  # 当前目录路径
        dirs  # 当前路径下所有子目录
        files  # 当前路径下所有非目录子文件
        :return:
        """
        for root, dirs, files in os.walk(self.download_path):
            if not files:
                return
            for file in files:
                dir_path = os.path.join(self.download_path, file)
                os.remove(dir_path)

    def upload(self):
        time.sleep(1)
        self.click_element('//span[text()="预算管理"]//parent::span//parent::div', mode='internal')
        self.click_element('//a[contains(@href,"budgetProgram")]', mode='internal')
        self.click_element('//span[text()="导入预算额"]//parent::button', mode='internal')
        time.sleep(0.5)
        self.click_element('//span[text()="上传完善后的表格文件"]//parent::button', mode='internal')

        # 输入文件名上传影像
        title = self.get_html_title()
        windows = Desktop(backend='uia')
        win = windows[f'{title} - Google Chrome']
        for root, dirs, files in os.walk(self.download_path):
            if not files:
                return
            for file in files:
                dir_path = os.path.join(self.download_path, file)
            if win.exists():
                editor = win.child_window(title="文件名(N):", control_type="Edit")
                editor.type_keys(dir_path, with_spaces=True)
                time.sleep(0.5)
                editor.type_keys("{ENTER}")
            time.sleep(1)
        self.click_element('//span[text()="确 定"]//parent::button', mode='internal')

    def get_html_title(self):
        title_xpath = 'html/head/title'
        ele = self.find_element(title_xpath)
        title = ele.get_attribute("textContent")
        return title

    def getDataFromWebPage(self):
        """
        从网页爬取数据
        :return:
        """
        self.click_element('//span[text()="公司管理"]//../..', mode='internal')
        # trs = self.driver.find_element_by_css_selector("table tr")
        tr_xpath = '//span[text()="公司全称"]/../../../../../../../../div[2]//table[@class="ant-table-fixed"]/tbody/tr'
        trs = self.find_elements(tr_xpath)
        # table tr
        data = []
        for i in range(len(trs)):
            company = {}
            for j in range(9):
                td = self.find_element(f"{tr_xpath}[{i + 1}]/td[{j + 1}]")
                # print(td.get_attribute('textContent'))
                if j == 0:
                    company['公司全称'] = td.get_attribute('textContent')
                elif j == 1:
                    company['公司简称'] = td.get_attribute('textContent')
                elif j == 2:
                    company['公司编码'] = td.get_attribute('textContent')
                elif j == 3:
                    company['公司类型'] = td.get_attribute('textContent')
                elif j == 4:
                    company['归属地'] = td.get_attribute('textContent')
                elif j == 5:
                    company['联系人'] = td.get_attribute('textContent')
                elif j == 6:
                    company['手机号'] = td.get_attribute('textContent')
                elif j == 7:
                    company['纳税人识别号'] = td.get_attribute('textContent')
                elif j == 8:
                    company['开户账户'] = td.get_attribute('textContent')
            data.append(company)

        print(data)
        names = json.dumps(data)
        self.filename = os.path.join(self.download_path, 'names.json')
        with open(self.filename, 'w') as file_obj:
            file_obj.write(json.dumps(data, ensure_ascii=False))

    def getDataFromJsonFile(self):
        """
        从json文件读取数据
        :return:
        """
        self.filename = os.path.join(self.download_path, 'names.json')
        with open(self.filename) as f:
            company_list = json.load(f)
        for company_dict in company_list:
            print(company_dict['公司全称'])
            print(company_dict['公司简称'])
            print(company_dict['公司编码'])
            print(company_dict['公司类型'])
            print(company_dict['归属地'])
            print(company_dict['联系人'])
            print(company_dict['手机号'])
            print(company_dict['纳税人识别号'])
            print(company_dict['开户账户'])





if __name__ == "__main__":
    if not os.path.exists(ENTRY_DOWNLOAD_PATH):
        os.mkdir(ENTRY_DOWNLOAD_PATH)
    DownLoadPath = os.path.join(ENTRY_DOWNLOAD_PATH, 'test')
    if not os.path.exists(DownLoadPath):
        os.mkdir(DownLoadPath)

    robot = YfysRobotTest(url=URL, download_path=DownLoadPath)
    robot.run()
