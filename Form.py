import sys
import time

from PyQt5.Qt import *

from DrawText import DrawText
from TWindow import TWindow


class Form(QMainWindow):
    def __init__(self, splash):
        super(Form, self).__init__()
        self.resize(8, 6)

        self.splash = splash

        # self.load_data()
        # self.splash.showMessage('测试', Qt.AlignCenter | Qt.AlignCenter, Qt.white)
    def load_data(self):
        for i in range(10):
            time.sleep(1)
            self.splash.showMessage(f'加载中...{str(i * 10)}%', Qt.AlignLeft | Qt.AlignBottom, Qt.white)


if __name__ == '__main__':
    # import sys
    # from PyQt5.QtWidgets import QApplication
    #
    # app = QApplication(sys.argv)
    #
    # splash = QSplashScreen()
    # # splash.setPixmap(QPixmap(r'D:\t.jpg'))  # 设置背景图片
    #
    # splash.setWindowOpacity(0.7)
    #
    # splash.setFont(QFont('微软雅黑', 100))  # 设置字体
    # # splash.showMessage('测试', Qt.AlignCenter | Qt.AlignCenter, Qt.white)
    #
    # splash.showMessage("程序启动画面", Qt.AlignLeft | Qt.AlignBottom, QColor(255, 255, 255))
    # splash.show()
    # # splash.showMessage('234')
    # app.processEvents()  # 处理主进程，不卡顿
    # form = Form(splash)
    # # form.show()
    # splash.finish(form)  # 主界面加载完成后隐藏
    # splash.deleteLater()
    # app.exec_()

    app = QApplication(sys.argv)
    window = DrawText()
    # window.sy()
    # window.wa()
    # window.cl()
    window.changeContent('yyy')
    window.changeCapacity(0.5)

    # time.sleep(5)
    # window.changeCapacity(0)
    # window.close()
    sys.exit(app.exec_())
    app = QApplication(sys.argv)
    window = DrawText()
    # window.sy()
    # window.wa()
    # window.cl()
    # time.sleep(5)
    window.changeContent('123')
    window.changeCapacity(0.5)
    # window.changeCapacity(0)
    # window.close()
    sys.exit(app.exec_())