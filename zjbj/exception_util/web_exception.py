# -*- coding: utf-8 -*-
# @Date    : 2021/11/5 11:11
# @File    : web_exception.py
# @User    : zhensu2
class LoginError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class TimeoutDownloadError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class EntryProcessError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

class TimeoutImageUploadError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg
