

# 开发第一个基于PyQt5的桌面应用

import sys
import time

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel


class TipWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.func()

    def initUI(self):

        # 设置窗口的标题
        # self.setWindowTitle('第一个基于PyQt5的桌面应用')

        # self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.CustomizeWindowHint )  # 窗体总在最前端
        # self.setWindowFlags(QtCore.Qt.CustomizeWindowHint)  # 去掉标题栏的代码
        # self.setStyleSheet('''QWidget{background-color:rgb(245, 245, 245);}''')
        self.setStyleSheet("#MainWindow{background-color: yellow}")

        self.center()
        # 设置半透明
        self.setWindowOpacity(1)

    def func(self):
        # 构造函数
        label = QLabel('PyQt5中文网', self)
        label.resize(200, 50)
        label.move(100, 100)
        label.setStyleSheet('background-color:green')
        label.setText("<a href='https://www.pyqt5.cn'>pyqt5中文网</a>")

    # 屏幕居中
    def center(self):
        # 获取窗口大小
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size = self.geometry()

        # 本窗体运动
        self.move(0, (screen.height() - size.height()) / 2)
        self.resize(screen.width(),240)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = TipWindow()
    # window.showMessage("程序启动画面", Qt.AlignLeft | Qt.AlignBottom, QColor(255, 255, 255))
    window.show()
    time.sleep(5)
    window.close()
    sys.exit(app.exec_())