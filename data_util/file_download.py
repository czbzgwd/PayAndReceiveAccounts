# _*_ coding:utf-8 _*_
""" 
@Time: 2021/11/11 15:35
@User: zhwang48
"""
import requests

from data_util.my_log import logger

def download(url, path):
    try:
        r = requests.get(url)
        if not r.status_code == 200:
            return False
        with open(path, "wb") as f:
            f.write(r.content)
        logger.info(f"{path}下载完成...")
        return True
    except:
        return False