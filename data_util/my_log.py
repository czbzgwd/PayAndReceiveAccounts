# _*_ coding:utf-8 _*_
"""
@Time: 2021/11/20 11:32
@User: zhwang48
"""
import logging, os
from logging import handlers


class MyLogger(object):
    level_relations = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'crit': logging.CRITICAL
    }  # 日志级别关系映射

    def __init__(self, filename, level='info', when='D', backCount=3,
                 fmt='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s'):
        """
            S 秒
            M 分
            H 小时、
            D 天、
            W 每星期（interval
            midnight 每天凌晨
        """
        self.logger = logging.getLogger(filename)
        format_str = logging.Formatter(fmt)  # 设置日志格式
        self.logger.setLevel(self.level_relations.get(level))  # 设置日志级别
        sh = logging.StreamHandler()  # 往屏幕上输出
        sh.setFormatter(format_str)  # 设置屏幕上显示的格式
        #         th = handlers.TimedRotatingFileHandler(filename=filename, when=when, backupCount=backCount,
        #                                                encoding='utf-8')  # 往文件里写入#指定间隔时间自动生成文件的处理器
        th = logging.FileHandler(filename, mode='a', encoding='utf-8')

        th.setFormatter(format_str)  # 设置文件里写入的格式
        self.logger.addHandler(sh)  # 把对象加到logger里
        self.logger.addHandler(th)


log_path = 'C:\RPA_LOG'
if not os.path.exists(log_path):
    os.mkdir(log_path)
file_path = os.path.join(log_path, 'CSCEC8SH_ENTRY_LOG.log')
default_level = 'info'
logger = MyLogger(filename=file_path, level=default_level).logger

