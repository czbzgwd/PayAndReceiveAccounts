# _*_ coding:utf-8 _*_
""" 
@Time: 2021/11/18 14:02
@User: zhwang48
"""
import base64, time, json, requests
from Crypto import Random
from Crypto.Cipher import PKCS1_v1_5 as Cipher_pkcs1_v1_5
from Crypto.PublicKey import RSA


class RSACipher(object):
    _private_pem = '-----BEGIN PRIVATE KEY-----\nMIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALI4e6wkhQoOUsLC\n0CR6yhuAlouGdqFQcnTjfIrezO8cwzXoJIqnl7RrmnQtr15Mu1kJ2wpij/QQAVve\nIUJi34dyQPfvPGP8wH7rD6CUb/buW/JZEsARO2j9d/EuJCVN3s45/cbm3TGIzqnc\ntBW6GvoXUb7dxO2fO6pSFW2kmrvrAgMBAAECgYEAkLhxJO5a4jmTVvgfUSAssnyO\nwyojdwb+ipCX96TIwXqXwlSKJ9XDyQx/1curVmX01G6+YgZ7YvMER3dvhpsHa3d+\nVF5llZ5vubHb1/TK+9ZzgBcmVc7bP24A6qrzzTsWq45hGsyQ2v5b8JZ35KmWz5nY\nKgLVHhrwUFGXqu68PLECQQDnp1vWBVwGbNPzRTnp69Xf0qnt4ja9t+SK35JZmlfm\nFGB3PQKy7hkbLEqk4Me0bOkW3BmU/Z154lcuX5/YThYVAkEAxPOB2XVHExif1Dv+\nqYBdRq+8ETkwx7zTvvQwK4ITc0JbzaP8I8q7HDfxKelSRHfFPpKV8aWqI312zeSF\nHtwJ/wJBAK98wf5U2wzm/n1I0QGbLc87+I0EFHIOT+zAR07ntHTQtO8/Yb8Izi4e\nlIFo4wCUjNNLc3nB0r0VsPOpbx7esK0CQCy0iVjvAf8Xq8/k/l1DQej1ot7V+Hie\nKAJEM1rs37Vd7rCy9Fy7e9Vf7WHyEDZyaVdWtqpvTDtaE0mRAZaLo8kCQGFN2Uwo\n7I/GLm2O97e+5Wkzq39PHr029+GqgG+qeuT+ds0Ow8nydNyYGSlLMExp4jixAZQT\nnUPNa0/2CvaPX8o=\n-----END PRIVATE KEY-----'
    _public_pem = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyOHusJIUKDlLCwtAkesobgJaL\nhnahUHJ043yK3szvHMM16CSKp5e0a5p0La9eTLtZCdsKYo/0EAFb3iFCYt+HckD3\n7zxj/MB+6w+glG/27lvyWRLAETto/XfxLiQlTd7OOf3G5t0xiM6p3LQVuhr6F1G+\n3cTtnzuqUhVtpJq76wIDAQAB\n-----END PUBLIC KEY-----"

    def __init__(self):
        _random_generator = Random.new().read
        _rsa = RSA.generate(1024, _random_generator)

    def get_public_key(self):
        return self._public_pem

    def get_private_key(self):
        return self._private_pem

    # def load_keys(self):
    #     with open('master-public.pem', "r") as f:
    #         self._public_pem = f.read()
    #     with open('master-private.pem', "r") as f:
    #         self._private_pem = f.read()
    #
    # def save_keys(self):
    #     with open('master-public.pem', 'wb') as f:
    #         f.write(self._public_pem)
    #     with open('master-private.pem', 'wb') as f:
    #         f.write(self._private_pem)

    def decrypt_with_private_key(self, _cipher_text):
        _rsa_key = RSA.importKey(self._private_pem)
        # print(self._private_pem)
        _cipher = Cipher_pkcs1_v1_5.new(_rsa_key)
        _text = _cipher.decrypt(base64.b64decode(_cipher_text), "ERROR")
        return _text.decode(encoding="utf-8")

    def encrypt_with_public_key(self, _text):
        _rsa_key = RSA.importKey(self._public_pem)
        _cipher = Cipher_pkcs1_v1_5.new(_rsa_key)
        _cipher_text = base64.b64encode(_cipher.encrypt(_text.encode(encoding="utf-8")))
        return _cipher_text

    def encrypt_with_private_key(self, _text):
        _rsa_key = RSA.importKey(self._private_pem)
        _cipher = Cipher_pkcs1_v1_5.new(_rsa_key)
        _cipher_text = base64.b64encode(_cipher.encrypt(_text.encode(encoding="utf-8")))
        return _cipher_text

    def decrypt_with_public_key(self, _cipher_text):
        _rsa_key = RSA.importKey(self._public_pem)
        _cipher = Cipher_pkcs1_v1_5.new(_rsa_key)
        _text = _cipher.decrypt(base64.b64decode(_cipher_text), "ERROR")
        return _text.decode(encoding="utf-8")

    def get_sign(self):
        time_str = str(time.time())
        sign = self.encrypt_with_public_key(time_str).decode()
        return sign

if __name__ == '__main__':
    # print(RSACipher().encrypt_with_private_key('20104208'))
    # print(RSACipher().encrypt_with_private_key('8023Myself.'))
    print(RSACipher().decrypt_with_private_key('QZr3sWfWiw6OP4wutS2WNQioDXtJL9prmMbJsyS6RV5SIrzwLpcqSgoTGZjvvmId42Q00Kng+5n3tzrw+9czHp+XJH+TRuOyssdvql5dRDDa0aNvneHoCKk25mtzTDHWfWMseT88Q+086wrhtrypZUQ2sMmj/JksGHLhldPeIEg='))
    print(RSACipher().decrypt_with_private_key('cfx8zdni0r9PMFHzuNGG8UlI6vgPJGe2Dn7g/4mPmrYRFU3YILcSU7gVIxBVWFLVKecwJF9FE/uZfGawl1yrn/aAqfeC3KSyq0eFAeAstko+HFNQFAVPsoStZQzQyoiCXHZW9h0Rm71VcTZ2ZJdSQmksYCjjBTR4o1O4b/g0Db8='))
